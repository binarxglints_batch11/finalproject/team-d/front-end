import React from 'react'
import './Landing.scss'
import Header from '../components/landing/Header';
// import CardCompanyValue from '../components/landing/CardCompanyValue';
import Card from '../components/landing/Card'
import Value from '../components/landing/Value'
import { landingObjOne } from '../components/landing/Value/Data'
import MainService from '../components/landing/MainService';
import MainBlogPost from '../components/landing/MainBlogPost';
import Footer from '../components/landing/Footer';
import MainCallToAction from '../components/landing/MainCallToAction';


const Landing = () => {
    return (
        <div>
            <Header/>
            <Value {...landingObjOne}/>
            <Card/>
            {/* <CardCompanyValue/> */}
            <MainService/>
            <MainBlogPost/>
            <MainCallToAction/>
            <Footer/>
        </div>
    )
}

export default Landing
