import React from 'react';
import Routes from './Routes/router';

function App() {
  return (
    <div>
      <Routes/>
    </div>
  );
}

export default App;
