import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Landing from '../pages/Landing';
import CustomerRegistration from '../pages/registration/CustomerRegistration';

const Routes = () => {
    
    return(
        <Router>
            <Switch>
                <Route path="/" exact>
                    <Landing /> 
                </Route>
                <Route path="/CustomerRegistration" exact>
                    <CustomerRegistration />  
                </Route>
            </Switch>
        </Router>
    );
}

export default Routes;