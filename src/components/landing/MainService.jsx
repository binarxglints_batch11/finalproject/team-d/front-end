import React from 'react'
import {Row, Col} from 'react-bootstrap'
import '../../pages/Landing.scss'
import Couch from '../assets/img/couch.png'
import Plug from '../assets/img/plug.png'
import Computer from '../assets/img/tv.png'
import Wash from '../assets/img/wash.png'
import Services from '../assets/img/services.png'

function MainService() {

    return (
    
    <div>
    <Row className="d-flex flex-wrap">
        <div className="title-container">
        <h1>Grow big with us</h1>
        </div>
        <Col>
            <Row className="services-container">
                <Col className="icon-services">
                    <div className="icons">
                        <img src={Computer} alt="computer-icon"/>
                    </div>
                    <h5>Computer services</h5>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus, expedita cupiditate velit facere dolores quidem modi obcaecati asperiores sequi, voluptate fugit similique, nihil ipsum voluptatibus quos maxime at optio maiores!
                </Col>
                <Col className="icon-services">
                    <div className="icons">
                        <img src={Wash} alt="appliances-icon"/>
                    </div>
                    <h5>Home appliances services</h5>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque cupiditate architecto facilis sapiente dignissimos recusandae ipsa doloremque quos eum reprehenderit nemo amet, autem porro consequatur officia ut corporis doloribus nostrum!
                </Col>
            </Row>
            <Row className="services-container">
                <Col className="icon-services">
                    <div className="icons">
                        <img src={Plug} alt="electronics-icon"/>
                    </div>
                    <h5>Electronics services</h5>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptas similique nulla cum enim quae voluptatem dolorum dolore, iste, consectetur eos possimus vero cumque reiciendis, doloribus at ex ut id earum.
                </Col>
                <Col className="icon-services">
                    <div className="icons">
                        <img src={Couch} alt="furniture-icon"/>
                    </div>
                    <h5>Furniture services</h5>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolorem itaque optio maiores amet quaerat qui. Non illum ipsa iure, perferendis ipsum rem aspernatur quaerat nisi atque aliquam fugit officiis eum!
                </Col>
            </Row>
        </Col>
        <Col className="services-image">
            <img src={Services} alt="pic"/>
        </Col>
    </Row>
    <a className="button">More Services</a>
    </div>
    
    
    
    // <div className="main-container">
    //     <h1 className="blueLandingSubtitle text-left">Grow big with us</h1>
    //     <div className="card-container">
    //         <Row className="card-set">
    //             <Col>
    //                 <Card style={{ width: '5rem', height: '10rem'}}>
    //                     <Card.Img variant="top" src={Computer} alt="comp-icon"/>
    //                     <Card.Body>
    //                         <Card.Title>Computer services</Card.Title>
    //                         < div style={{marginBottom: '30px'}}></div>
    //                         <Card.Text>
    //                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim vel odio nostrum autem saepe, deserunt quibusdam maxime, quae dolores, expedita consectetur. Iste vel quae ea autem architecto eum dignissimos soluta?
    //                         </Card.Text>
    //                     </Card.Body>
    //                 </Card>
    //             </Col>

    //             <Col>
    //                 <Card style={{ width: '5rem', height: '10rem'}}>
    //                     <Card.Img variant="top" src={Wash} alt="wash-icon"/>
    //                     <Card.Body>
    //                         <Card.Title>Home appliances services</Card.Title>
    //                         < div style={{marginBottom: '30px'}}></div>
    //                         <Card.Text>
    //                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim vel odio nostrum autem saepe, deserunt quibusdam maxime, quae dolores, expedita consectetur. Iste vel quae ea autem architecto eum dignissimos soluta?
    //                         </Card.Text>
    //                     </Card.Body>
    //                 </Card>
    //             </Col>
    //         </Row>
    //     </div>

    //     <div className="card-container">
    //         <Row style={{marginLeft: '40px', marginRight: '12px'}}>
    //             <Col>
    //                 <Card style={{ width: '5rem', height: '10rem'}}>
    //                     <Card.Img variant="top" src={Plug} alt="plug-icon"/>
    //                     <Card.Body>
    //                         <Card.Title>Electronic services</Card.Title>
    //                         < div style={{marginBottom: '30px'}}></div>
    //                         <Card.Text>
    //                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim vel odio nostrum autem saepe, deserunt quibusdam maxime, quae dolores, expedita consectetur. Iste vel quae ea autem architecto eum dignissimos soluta?
    //                         </Card.Text>
    //                     </Card.Body>
    //                 </Card>
    //             </Col>

    //             <Col>
    //                 <Card style={{ width: '5rem', height: '10rem'}}>
    //                     <Card.Img variant="top" src={Couch} alt="couch-icon"/>
    //                     <Card.Body>
    //                         <Card.Title>Furniture services</Card.Title>
    //                         < div style={{marginBottom: '30px'}}></div>
    //                         <Card.Text>
    //                             Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim vel odio nostrum autem saepe, deserunt quibusdam maxime, quae dolores, expedita consectetur. Iste vel quae ea autem architecto eum dignissimos soluta?
    //                         </Card.Text>
    //                     </Card.Body>
    //                 </Card>
    //             </Col>
    //         </Row>

    //     </div>
    //     <div className="container-fluid postMoreServices text-left">
    //         <Button variant="outline-success">More Services</Button>
    //     </div>
    // </div>
    )
}

export default MainService;