export const landingObjOne = {
    id: 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Company Value',
    headline: `Here's our company values`,
    description: 'Lorem ipsum dolor sit amet',
    buttonLabel : 'Join Us',
    imgStart: false,
    dark: true,
    primary: true,
    darkText: true
}