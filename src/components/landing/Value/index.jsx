import React from 'react'
import {Button} from '../ButtonElm'
import {ValueContainer, 
    ValueWrapper, 
    ValueRow, 
    Column1, 
    TextWrapper, 
    Heading, 
    Sub, 
    BtnValue, 
    Column2, 
    ImgWrap,
    TopLine} from './ValueElm'

const Value = ({lightBg, id, imgStart, topLine, lightText, headline, darkText, description,
buttonLabel, primary, dark, anotherdark}) => {
    return (
        <>
         <ValueContainer lightBg={lightBg} id={id}>
             <ValueWrapper>
                 <ValueRow imgStart={imgStart}>
                     <Column1>
                     <TextWrapper>
                        <TopLine>{topLine}</TopLine>
                        <Heading lightText={lightText}>{headline}</Heading>
                        <Sub darkText={darkText}>{description}</Sub>
                     </TextWrapper>
                     
                     
                     <BtnValue>
                         <Button to="landing"
                         smooth={true}
                         duration={500}
                         spy={true}
                         exact="true"
                         offset={-80}
                         primary={primary ? 1 : 0}
                         dark={dark ? 1 : 0}
                         anotherdark={dark ? 1 : 0}
                         >{buttonLabel}</Button>
                     </BtnValue>
                     </Column1>
                     <Column2>
                        <ImgWrap>
                        {/* <Img src={img} alt={alt}/> */}
                        </ImgWrap>
                     </Column2>
                 </ValueRow>
             </ValueWrapper>
             </ValueContainer>   
        </>
    )
}

export default Value
