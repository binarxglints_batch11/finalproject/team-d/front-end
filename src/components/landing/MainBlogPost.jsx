import React from 'react'
import {Container, Card, Row, Col, Button} from 'react-bootstrap';
import blogpost1 from '../assets/img/blogpost1-img.png';
import blogpost2 from '../assets/img/blogpost2-img.png';
import blogpost3 from '../assets/img/blogpost3-img.png';
import blogpost4 from '../assets/img/blogpost4-img.png';
import blogpost5 from '../assets/img/blogpost5-img.png';
import blogpost6 from '../assets/img/blogpost6-img.png';


const MainBlogPost = () => {
    return (
        <div className="container">
            <h1 className="blueLandingSubtitle text-center">Latest Blog Update</h1>
            <div className="card-blog">
            <Row style={{marginLeft: '12px', marginRight: '12px'}}> 
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost1} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                            This is a wider card with supporting text below as a natural lead-in to
                            additional content. This content is a little bit longer.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost2} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, 
                            auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost3} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, 
                            auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            </div>

            <div className="card-blog">
            <Row style={{marginLeft: '12px', marginRight: '12px'}}> 
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost4} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, 
                        auctor sit amet aliquam vel, ullamcorper sit amet ligula..
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost5} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, 
                        auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col>
                    <Card style={{ width: '20rem', height: '30rem'}}>
                        <Card.Img variant="top" src={blogpost6} alt="blog-img"/>
                        <Card.Body>
                        <Card.Title>Blog Post Title 1</Card.Title>
                        <div style={{marginBottom: '30px'}}><small className="text-muted">29 June 2021</small></div>
                        <Card.Text>
                        Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                        </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            
            </div>
            <div className="container-fluid postReadMore text-center">
                <Button variant="outline-success">Read More</Button>
            </div>
        </div>
    )
}

export default MainBlogPost
