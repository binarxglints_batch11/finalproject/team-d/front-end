import React from 'react'
import {Image, Container, Navbar, Nav, NavDropdown} from 'react-bootstrap'
import bannerImg from '../assets/img/banner.png'
import '../../pages/Landing.scss'

const Header = () => {

    return (
        <div>
                <div>
                   <nav className="d-flex flex-wrap navigation">
                       <Container className="navbar-landing">
                       <Navbar collapseOnSelect expand="lg" variant="dark">
                                <div>
                                    <Navbar.Brand className="logo" style={{color: 'white', position: 'absolute', top: '20px', fontSize: '30px', fontFamily: 'Lato'}}>
                                    Tech stop
                                    </Navbar.Brand>
                                </div>
                       <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse>   
                            <Nav>
                                <div>
                                    <Nav.Link className="about" style={{color: 'white', fontSize: '20px', fontFamily: 'Lato'}}>
                                    About
                                    </Nav.Link>
                                    <Nav.Link className="career" style={{color: 'white',  fontSize: '20px', fontFamily: 'Lato'}}>
                                    Career
                                    </Nav.Link>
                                    <Nav.Link className="blog" style={{color: 'white', fontSize: '20px', fontFamily: 'Lato'}}>
                                    Blog
                                    </Nav.Link>
                                </div>
          
                                <div className="navigation">
                                    <NavDropdown.Item  className="button-auth partnerBtn" href="#"  style={{color: 'white', fontSize: '15px', fontFamily: 'Lato'}}>
                                    Register as Partner
                                    </NavDropdown.Item>
                                    <NavDropdown.Item  className="button-auth customerBtn" href="CustomerRegistration" style={{color: 'white', position: 'absolute', fontSize: '15px', fontFamily: 'Lato'}}>
                                    Register as Customer
                                    </NavDropdown.Item>
                                </div>
                       
                            </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                       </Container>
                   </nav>
                    <div className="subtitleLanding">
                        <div className="d-flex flex-wrap sm={1} md={2} lg={4}">
                            <a style={{color: 'white', position: 'absolute', top: '250px', left: '10vw', fontSize: '40px', fontFamily: 'Mulish', marginTop: '20px'}} className='headerText-1'>
                            There is always someone to help with your problem
                            </a>
                            <a style={{color: 'white', position: 'absolute', top: '330px', left: '10vw', fontSize: '20px', fontFamily: 'Mulish'}} className='headerText-2'>
                            Et has minim elitr intellegat. Mea aeterno eleifend antiopam ad, nam no suscipit quaerendum.<br/> At nam minimum ponderum. Est audiam animal molestiae te.
                            </a>
                        </div>
                    </div>
                </div>
                <Image src={bannerImg} style={{position: 'flexible'}}/>
                </div>
    
    )
}

export default Header;
