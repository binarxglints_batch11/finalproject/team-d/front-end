import styled from 'styled-components'

export const CardContainer = styled.div`
 height: 800px;
 display: flex;
 flex-direction: column;
 justify-content: center;
 align-items: center;

 background: none;

 @media screen and (max-width: 768px) {
     height: 1100px
 }

 @media screen and (max-width: 480px) {
     height: 1300px
 }
`

export const CardWrapper = styled.div`
 max-width: 1000px;
 //margin: 0 auto;
 margin-top: -950px;
 display: grid;
 grid-template-columns: 1fr 1fr 1fr 1fr;
 align-items: center;
 grid-gap: 16px;
 padding: 0 50px;

 @media screen and (max-width: 1000px) {
     grid-template-columns: 1fr 1fr;
 }
 @media screen and (max-width: 768px) {
     grid-template-columns: 1fr;
     padding: 0 20px;
 }
`
export const CardValue = styled.div`
background: linear-gradient(0deg, #0796C6 -22.62%, #50E590 115.83%);
 display: flex;
 flex-direction: column;
 justify-content: flex-start;
 align-items: center;
 border-radius: 10px;
 height: 340px;
 padding: 30px;
 box-shadow: 0 1px 3px rgba(0,0,0,0.2);
 transition: all 0.2s ease-in-out;


 &:hover {
     trasnform: scale(1.02);
     transition: all 0.2s ease-in-out;
     cursor: pointer;
 }
`

export const CardIcon = styled.img`
 height: 160px;
 width: 160px;
 margin-bottom: 10px;
`
export const CardH2 = styled.h2`
 font-size: 1rem;
 color: #fff;
 margin-bottom: 14px;
 
`
export const CardP = styled.p`
 font-size: 1rem;
 text-align: center;
`

