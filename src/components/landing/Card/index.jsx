import React from 'react'
import {CardContainer, CardWrapper, CardValue, CardIcon,
CardH2, CardP} from '../Card/CardElm'
import Icon1 from '../../assets/img/Saly-1.png'
import Icon2 from '../../assets/img/Saly-14.png'
import Icon3 from '../../assets/img/Saly-1.png'
import Icon4 from '../../assets/img/Saly-15.png'

const Card = () => {
    return (
        <CardContainer>
            <CardWrapper>
                <CardValue>
                    <CardIcon src={Icon1}/>
                    <CardH2>Company Value 1</CardH2>
                    <CardP>Lorem ipsum dolor sit amet</CardP>
                </CardValue>
                <CardValue>
                    <CardIcon src={Icon2}/>
                    <CardH2>Company Value 2</CardH2>
                    <CardP>Lorem ipsum dolor sit amet</CardP>
                </CardValue>
                <CardValue>
                    <CardIcon src={Icon3}/>
                    <CardH2>Company Value 3</CardH2>
                    <CardP>Lorem ipsum dolor sit amet</CardP>
                </CardValue>
                <CardValue>
                    <CardIcon src={Icon4}/>
                    <CardH2>Company Value 4</CardH2>
                    <CardP>Lorem ipsum dolor sit amet</CardP>
                </CardValue>
            </CardWrapper>
        </CardContainer>
    )
}

export default Card
