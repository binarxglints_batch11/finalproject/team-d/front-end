import React from 'react'
import './footer.scss'
import Appstore from '../assets/img/apple.png'
import Playstore from '../assets/img/pstore.png'

// something

const Footer = () => {
    return (
    <div className="footer-mini">
      <div className="footer-mini-top">
        <div className="footer-mini-top-1">
          <div className="footer-mini-top-1-1">
            <h2>TECH STOP</h2>
          </div>
          <div className="footer-mini-top-1-2">
            Copyright © 2021. All Rights Reserved
          </div>
        </div>
        <div className="footer-mini-top-2">
          <div className="footer-mini-top-2-1">
            <div className="footer-mini-top-2-1-company">Company</div>
            <div className="footer-mini-top-2-about">About</div>
            <div className="footer-mini-top-2-blog">Career</div>
            <div className="footer-mini-top-2-media">Blog</div>
          </div>
        </div>
        <div className="footer-mini-top-3">
          <div className="footer-mini-top-3-1">
            <div className="footer-mini-top-3-1-touch">Get in touch</div>
            <div className="footer-mini-top-3-help">Help</div>
            <div className="footer-mini-top-3-contact">Contact</div>
          </div>
        </div>
        <div className="footer-mini-top-4">
          <div className="footer-mini-top-4-1">
            <div className="footer-mini-top-4-1-download">Download</div>
            <div className="footer-mini-top-4-1-image1">
              <img src={Playstore} alt="gplay" />
            </div>
            <div className="footer-mini-top-4-1-image2">
              <img src={Appstore} alt="apple" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
