import React from 'react'
import { Container, Image, Row, Col } from 'react-bootstrap';
import PlaystoreBtn from '../assets/img/playstore.png'
import AppstoreBtn from '../assets/img/appstore.png'
import Mockup from '../assets/img/Mockup.png'


const MainCallToAction = () => {
    return (
        <Container>
            <Row className="landingCta justify-content-center">
            <h1 className="blackLandingSubtitle">Download App</h1>
                <Col class>                    
                    <p>Sed porttitor lectus nibh. Nulla porttitor accumsan tincidunt. 
                    Vivamus suscipit tortor eget felis porttitor volutpat. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada. 
                    Mauris blandit aliquet elit, eget tincidunt.
                    </p>
                    <div className="d-flex flex-wrap gap-3 align-item-center">
                        <Image src={PlaystoreBtn}/>
                        <Image src={AppstoreBtn}/>
                    </div>
                </Col>

                <Col>
                    <Image src={Mockup} />
                </Col>
            </Row>

        </Container>
    )
}

export default MainCallToAction
